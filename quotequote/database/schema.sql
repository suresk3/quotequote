drop table if exists quote cascade;
drop table if exists quote_tag cascade;
drop table if exists global_tag cascade;
drop table if exists global_quote cascade;
drop table if exists m_user cascade;
drop table if exists recommendation cascade;
drop table if exists friend cascade;

-- Quotes are the text we're storing. It could be a comment or a recipe or a review or a joke... whatever is interesting to the user.
create table quote (
  quote_id                  bigserial not null,
  owner_user_id             bigint default 0,          -- The use this quote belongs to
  name                      varchar(255) default '',  -- Simple identification for this quote. Like a recipe name.
  s_tags                    varchar(6000) default '',  -- A hashtag deliminated list of tags, such as '#inspirational #famous'
  quote_text								varchar(6000) default '',  -- The text to save and display
  description               varchar(6000) default '',  -- User specific comment
  url                       varchar(255) default '',  -- URL the quote came from.
	attributed_to             varchar(1000) default '',  -- More information on where this quote came from
	image                     varchar(1000) default null,  -- What image should backdrop this quote
	layout                    varchar(1000) default null,  -- Where the text should be display. Top? Bottom? Left? Right?
	sound                     varchar(1000) default null,  -- What sound should play when this text is shown.
  interest                  integer default 5,  -- How interested the user is in this quote.
  creation_date             bigint default 0,  -- When this quote was created.
  last_modified_date        bigint default 0,  -- When this quote was last modified
  last_viewed_date          bigint default 0,  -- When this quote was last viewed
  times_viewed              integer default 0,  -- Number of times the quote was viewed
	mood                      varchar(6000) default null,  -- The user's mood when they last viewed this quote
	copied_from               bigint default 0,  -- The global_quote object this quote was first copied from
  is_private                boolean default false,  -- Whther or not other users can view this quote
  constraint pk_quote primary key (quote_id)
);
create index ix_quote_owner_1 on quote (owner_user_id);

-- Information about available tags
create table quote_tag (
  quote_tag_id              bigserial not null,
  owner_user_id             bigint,
  tag                       varchar(255),
  constraint pk_quote_tag primary key (quote_tag_id)
);
create index ix_quote_tag_owner_2 on quote_tag (owner_user_id);

-- Quotes are the text we're storing. It could be a comment or a recipe or a review or a joke... whatever is interesting to the user.
create table global_quote (
  global_quote_id           bigserial not null,
  quote_text								varchar(6000) default '',  -- The text to save and display
  url                       varchar(255) default '',  -- URL the quote came from.
	attributed_to             varchar(1000) default '',  -- More information on where this quote came from
  creation_date             bigint default 0,  -- When this quote was created.
  times_viewed              integer default 0,  -- Number of times the quote was viewed
  constraint pk_quote primary key (global_quote_id)
);

-- Information about available tags
create table global_quote_tag (
  global_quote_tag_id       bigserial not null,
  owner_user_id             bigint,
  tag                       varchar(255),
  constraint pk_quote_tag primary key (global_quote_tag_id)
);

-- Recommendations per User
create table recommendation (
  recommendation_id          bigserial not null,
  target_user_id             bigint default 0,
  global_quote_id            bigint default 0,
  json                       varchar(6000) default '',
  creation_date              bigint default 0,
  constraint pk_recommendation primary key (recommendation_id)
);
create index ix_recommendation_target on recommendation (target_user_id);

-- Internal User Information
create table m_user (
  user_id                   bigserial not null,
  email                     varchar(255),
  username                  varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  last_login                timestamp,
  created                   timestamp,
  expiration                timestamp,
  active                    boolean,
  email_validated           boolean,
  preferences               TEXT,
  last_login_attempt        timestamp,
  constraint uq_m_user_email unique (email),
  constraint pk_m_user primary key (user_id)
);

-- Information on friends. These friends may not be users of the app, recommendations could be shared via email or sms
create table friend (
  friend_id         bigserial not null,
  owner_user_id     bigint default 0,
  friend_user_id    bigint default 0,
	email             varchar(255),
	sms               varchar(20),
  creation_date     bigint default 0,
  constraint pk_friend primary key (friend_id)
);